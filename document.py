# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import os
import time
import datetime
from threading import Thread
import subprocess
from trytond.pool import Pool
from trytond.model import ModelView, ModelSQL, Workflow, fields
from trytond.pyson import Eval
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, Button, StateTransition, StateReport

__all__ = [
    'Document', 'DocumentRecord', 'TypeDocument',
    'DocumentRecordInventory', 'DocumentRecordInventoryStart',
    'DocumentRecordInventoryReport', 'TRDReport', 'TRDStart', 'TRD',
    'QueryDocument', 'DocumentSelectAsk', 'DocumentSelect',
]

_STATES = {'readonly': Eval('state') != 'draft'}


def fmt_time(val):
    res = time.strftime("%a, %d %b %Y, %H:%M:%S", time.localtime(val))
    return res


class DocumentRecord(Workflow, ModelSQL, ModelView):
    'Document Record'
    __name__ = 'document.record'
    sequence = fields.Char('Sequence', readonly=True, states=_STATES)
    name = fields.Char('Name', required=True, states=_STATES)
    code = fields.Char('Code', readonly=True, select=True)
    catalog_number = fields.Char('Catalog Number', select=True,
            states=_STATES)
    folios_num = fields.Function(fields.Integer("Folios Number"),
            'get_folios_number')
    series = fields.Many2One('document.category', 'Series', domain=[
            ('kind', 'in', ['series', 'subseries']),
        ], states=_STATES, required=True)
    documents = fields.One2Many('document.document', 'record',
            'Documents', states=_STATES)
    location = fields.Many2One('document.location', 'Location',
            states=_STATES)
    physical_location = fields.Many2One('document.location', 'Location',
            states=_STATES)
    shelf = fields.Char('Shelf', select=True)
    panel = fields.Char('Panel', select=True)
    final = fields.Selection([
            ('conservation', 'Conservation'),
            ('elimination', 'Elimination'),
            ('digital', 'Digital'),
            ('selection', 'Selection'),
            ], 'Final', required=True)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('paperwork', 'Paperwork'),
            ('central', 'Central'),
            ('historic', 'Historic'),
            ], 'State', required=True, readonly=True)
    # Conservation Place
    place_box = fields.Char('Box', select=True, states=_STATES)
    place_folder = fields.Char('Folder', select=True, states=_STATES)
    place_volume = fields.Char('Volume', select=True, states=_STATES)
    place_other = fields.Char('Other', select=True, states=_STATES)
    notes = fields.Text('Notes', states=_STATES)
    storage_medium = fields.Selection([
        ('paper', 'Paper'),
        ('electronic', 'Electronic'),
        ('mixed', 'Mixed'),
        ('other', 'Other'),
        ], 'Storage Mediums', required=True, states=_STATES)
    storage_medium_string = storage_medium.translated('storage_medium')

    @classmethod
    def __setup__(cls):
        super(DocumentRecord, cls).__setup__()
        cls._order.insert(0, ('code', 'ASC'))
        # cls._error_messages.update({
        #     'documents_in_draft': 'There are documents in draft state!',
        #     'not_updated_directory': 'It is not possible update record: %s',
        #     'not_possible_create_dir': 'It is not possible create directory: %s',
        #     'directory_no_exists': 'Directory does not exists!',
        #     'directory_no_empty': 'Directory not empty!',
        # })
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state') != 'paperwork',
            },
            'paperwork': {
                'invisible': Eval('state').in_(['paperwork', 'historic']),
            },
            'central': {
                'invisible': Eval('state') != 'paperwork',
            },
            'historic': {
                'invisible': Eval('state') != 'central',
            },
            'document_select': {
                'invisible': Eval('state') != 'draft',
            },
        })
        cls._transitions |= set((
            ('draft', 'paperwork'),
            ('paperwork', 'central'),
            ('paperwork', 'draft'),
            ('central', 'paperwork'),
            ('central', 'historic'),
        ))

    def get_folios_number(self, name):
        return len(self.documents)

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_final():
        return 'conservation'

    @staticmethod
    def default_storage_medium():
        return 'paper'

    @classmethod
    @ModelView.button_action('document_collection.wizard_document_select')
    def document_select(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('paperwork')
    def paperwork(cls, records):
        for record in records:
            record.set_sequence()
            record.set_code()
            for doc in record.documents:
                if doc.state == 'draft':
                    cls.raise_user_error('documents_in_draft',)
                doc.set_code()

    @classmethod
    @ModelView.button
    @Workflow.transition('central')
    def central(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('historic')
    def historic(cls, records):
        pass

    def set_sequence(self):
        if self.sequence or not self.series or not self.series.code:
            return
        next_seq = self.series.get_next_number()
        self.write([self], {'sequence': next_seq})

    def set_code(self):
        if not self.series or not self.series.code or not self.sequence:
            return
        code = self.series.code + '.' + self.sequence
        self.write([self], {'code': code})

    @classmethod
    def create(cls, values):
        for value in values:
            cls._create_directory(value)
        record_ids = super(DocumentRecord, cls).create(values)
        return record_ids

    @classmethod
    def _create_directory(cls, val):
        Serie = Pool().get('document.category')
        series = Serie(val['series'])
        path_dir = os.path.join(series.path, val['name']).encode('utf-8')

        if not os.path.exists(path_dir):
            try:
                os.mkdir(path_dir, 0755)
            except Exception as e:
                cls.raise_user_error('not_possible_create_dir', e)

    @classmethod
    def write(cls, records, values):
        for rec in records:
            if values.get('name') or values.get('series'):
                rec._update_directory(values.get('name'), values.get('series'))
        super(DocumentRecord, cls).write(records, values)

    def _update_directory(self, name, series):
        Serie = Pool().get('document.category')

        if series:
            path_serie = Serie(series).path
        else:
            path_serie = self.series.path
        if not name:
            name = self.name

        current_path = os.path.join(self.series.path, self.name)
        new_path = os.path.join(path_serie, name)
        if os.path.exists(current_path):
            try:
                os.rename(current_path, new_path)
            except Exception as e:
                self.raise_user_error('not_updated_directory', e)
        else:
            self.raise_user_error('directory_no_exists')

    @classmethod
    def delete(cls, records):
        for record in records:
            record._delete_directory()
        super(DocumentRecord, cls).delete(records)

    def _delete_directory(self):
        path_dir = os.path.join(self.series.path, self.name)
        if os.path.exists(path_dir):
            try:
                os.rmdir(path_dir)
            except Exception as e:
                if e.errno == 39:
                    self.raise_user_error('directory_no_empty')
                else:
                    self.raise_user_error('not_possible_remove_dir', e)


class Document(Workflow, ModelSQL, ModelView):
    'Document'
    __name__ = 'document.document'
    name = fields.Char('Name', select=True, required=True, states={
        'readonly': True,
    })
    code = fields.Char("Code", readonly=True, select=True)
    sequence = fields.Char('Sequence', select=True)
    record = fields.Many2One('document.record', 'Record', select=True,
            ondelete='CASCADE', states=_STATES)
    type_document = fields.Many2One('document.type_document', 'Type',
            select=True, states=_STATES)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('posted', 'Posted'),
        ], 'State', required=True, readonly=True)
    date_post = fields.Date('Date Post', select=True)
    link = fields.Function(fields.Char('File Link'), 'get_link')
    path = fields.Char('Path', states={'readonly': True})
    description = fields.Text('Description', states=_STATES)
    category = fields.Function(fields.Many2One('document.category', 'Series'),
        'get_category')
    mtd_type = fields.Function(fields.Char('Type'), 'get_mtd')
    mtd_size = fields.Function(fields.Char('Size (Kb)'), 'get_mtd')
    mtd_accessed = fields.Function(fields.Char('Accessed'), 'get_mtd')
    mtd_modified = fields.Function(fields.Char('Modified'), 'get_mtd')
    lending = fields.Many2One('document.lending', 'Lending',
        select=True, states={'readonly': True})
    place_box = fields.Char('Box', select=True)
    place_folder = fields.Char('Folder', select=True)
    place_volume = fields.Char('Volume', select=True)
    place_other = fields.Char('Other', select=True)

    @classmethod
    def __setup__(cls):
        super(Document, cls).__setup__()
        cls._order.insert(0, ('place_folder', 'ASC'))
        # cls._error_messages.update({
        #     'missing_path_dms': 'Missing path to DMS on config file!',
        # })
        cls._buttons.update({
            'draft': {
                'invisible': True,
            },
            'post': {
                'invisible': Eval('state') == 'posted',
            },
        })
        cls._transitions |= set((
            ('draft', 'posted'),
        ))

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_date_post():
        return datetime.date.today()

    @classmethod
    def search_category(cls, name, clause):
        field_name = 'category'
        return [(field_name, clause[1], clause[2])]

    def get_mtd(self, name):
        res = None
        if not os.path.exists(self.link):
            return
        file_ = os.stat(self.link)
        if name == 'mtd_type':
            res = subprocess.check_output(['file', '-b', self.link])
            res = res.replace(b'\n', b'')
        elif name == 'mtd_size':
            # Convert bytes to kilobytes
            res = str(round((file_.st_size / 1024.0), 2))
        elif name == 'mtd_accessed':
            res = fmt_time(file_.st_atime)
        elif name == 'mtd_modified':
            res = fmt_time(file_.st_mtime)
        if type(res) == bytes:
            res = res.decode('utf-8')
        return res

    @classmethod
    @ModelView.button
    @Workflow.transition('posted')
    def post(cls, documents):
        for doc in documents:
            # doc.set_sequence()
            pass

    def set_sequence(self):
        next_seq = 1
        sequences = [doc for doc in self.record.documents if doc.state == 'posted']

        if sequences:
            next_seq = len(sequences) + 1
        self.write([self], {'sequence': str(next_seq)})

    def set_code(self):
        if self.code:
            return
        if self.record:
            code = self.record.code + '.' + str(self.sequence)
            self.write([self], {'code': code})

    def get_category(self, name):
        if self.record.series:
            return self.record.series.id

    def get_link(self, name):
        Configuration = Pool().get('document.configuration')
        config = Configuration.get_configuration()
        path_company = config.company_name_path
        if not self.record or not self.name:
            return
        res = os.path.join(config.path_localhost, path_company,
            self.record.series.rec_name, self.record.name, self.name)
        return res


class TypeDocument(ModelSQL, ModelView):
    'Type Document'
    __name__ = 'document.type_document'
    name = fields.Char('Name', required=True)


class DocumentRecordInventoryStart(ModelView):
    'Document Record Inventory Start'
    __name__ = 'document.record_inventory.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    section = fields.Many2One('document.category', 'Section', domain=[
            ('kind', 'in', ['section', 'subsection']),
        ], required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class DocumentRecordInventory(Wizard):
    'Document Record Inventory'
    __name__ = 'document.record.inventory'
    start = StateView('document.record_inventory.start',
        'document_collection.record_inventory_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('document.record.inventory_report')

    def do_print_(self, action):
        data = {
            'company': self.start.company.id,
            'section': self.start.section.id,
            }
        return action, data

    def transition_print_(self):
        return 'end'


class DocumentRecordInventoryReport(Report):
    'Document Record Inventory'
    __name__ = 'document.record.inventory_report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(DocumentRecordInventoryReport, cls).get_context(
            records, data)

        pool = Pool()
        DocumentRecord = pool.get('document.record')
        Company = pool.get('company.company')
        Category = pool.get('document.category')
        category = Category(data['section'])
        if category.kind == 'subsection':
            section = category.parent
        else:
            section = category
        subsection = category
        records = DocumentRecord.search([])
        report_context['records'] = records
        report_context['company'] = Company(data['company'])
        report_context['section'] = section
        report_context['subsection'] = subsection
        return report_context


class TRDStart(ModelView):
    'Tabla de Retencion Documental'
    __name__ = 'document.record_trd.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    section = fields.Many2One('document.category', 'Section', domain=[
            ('kind', 'in', ['section', 'subsection']),
        ])

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class TRD(Wizard):
    'Tabla de Retencion Documental'
    __name__ = 'document.record_trd'
    start = StateView('document.record_trd.start',
        'document_collection.record_trd_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('document.record.trd_report')

    def do_print_(self, action):
        data = {
            'company': self.start.company.id,
            }
        if self.start.section:
            data['section'] = self.start.section.id
        else:
            data['section'] = None
        return action, data

    def transition_print_(self):
        return 'end'


class TRDReport(Report):
    __name__ = 'document.record.trd_report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(TRDReport, cls).get_context(records, data)

        pool = Pool()
        Company = pool.get('company.company')
        DocumentRecord = pool.get('document.record')
        Category = pool.get('document.category')

        dom_values = []
        if data['section']:
            dom_values = [('id', '=', data['section'])]

        objects = []
        for obj in Category.search(dom_values):
            records = DocumentRecord.search([
                ('series.parent', '=', obj.id),
            ])
            setattr(obj, 'records', records)
            objects.append(obj)
        report_context['records'] = objects
        report_context['company'] = Company(data['company'])
        report_context['today'] = datetime.date.today()
        return report_context


class QueryDocument(Workflow, ModelSQL, ModelView):
    'Query Document'
    __name__ = 'document.query_document'
    document = fields.Many2One('document.document', 'Document',
            required=True, states={
                'readonly': Eval('state') != 'draft',
            })
    employee = fields.Many2One('company.employee', 'Employee',
        required=True, states={
            'readonly': Eval('state') != 'draft',
        })
    state = fields.Selection([
            ('draft', 'Draft'),
            ('done', 'Done'),
        ], 'State', required=True, readonly=True)
    date_query = fields.Date('Date Query', select=True,
            states={
                'readonly': Eval('state') != 'draft',
            })

    @classmethod
    def __setup__(cls):
        super(QueryDocument, cls).__setup__()
        cls._order.insert(0, ('date_query', 'DESC'))
        cls._buttons.update({
            'draft': {
                'invisible': True,
            },
            'done': {
                'invisible': Eval('state') == 'done',
            },
        })
        cls._transitions |= set((
            ('draft', 'done'),
        ))

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_date_query():
        return datetime.date.today()

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, records):
        pass


class DocumentSelectAsk(ModelView):
    'Document Select Assistant'
    __name__ = 'document.document.select.ask'
    name = fields.Char('Name', required=True, states={
        'readonly': True,
    })
    filedata = fields.Binary('File', filename='name', loading='eager')


class DocumentSelect(Wizard):
    'Document Select'
    __name__ = 'document.document.select'
    start = StateView('document.document.select.ask',
        'document_collection.document_select_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Add', 'add_doc', 'tryton-ok', default=True),
        ])
    add_doc = StateTransition()

    def transition_add_doc(self):
        Record = Pool().get('document.record')
        config = Pool().get('document.configuration')(1)
        ids = Transaction().context['active_ids']
        path_home = config.path_home
        if ids:
            record = Record(ids[0])
            path = os.path.join(
                record.series.path,
                record.name,
            )
            full_path = os.path.join(
                path_home,
                path,
                self.start.name,
            )
            thread_ = WriteTest(self.start.filedata, full_path)
            thread_.start()
            Record.write([record], {'documents': [
                ('create', [{
                    'name': self.start.name,
                }]),
            ]})
        return 'end'


class WriteTest(Thread):
    def __init__(self, value, full_path):
        Thread.__init__(self)
        self.value = value
        self.full_path = full_path

    def run(self):
        #t0 = time.time()
        with open(self.full_path, 'wb') as file_p:
            file_p.write(self.value)
            file_p.close()
            #d = (time.time() - t0) * 100
            #print "duration reading: %.2f ms." % d
