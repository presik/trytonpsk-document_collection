# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .document import (Document, DocumentRecord, TypeDocument,
    DocumentRecordInventory, DocumentRecordInventoryStart,
    DocumentRecordInventoryReport, TRDReport, TRDStart, TRD,
    QueryDocument, DocumentSelectAsk, DocumentSelect)
from .location import (DocumentLocation)
from .configuration import Configuration
from .category import CategoryDocument
from .lending import (DocumentLending, DocumentLendingDocument)


def register():
    Pool.register(
        TypeDocument,
        CategoryDocument,
        DocumentLocation,
        DocumentRecord,
        DocumentLending,
        Document,
        Configuration,
        DocumentLendingDocument,
        DocumentRecordInventoryStart,
        TRDStart,
        QueryDocument,
        DocumentSelectAsk,
        module='document_collection', type_='model')
    Pool.register(
        DocumentRecordInventoryReport,
        TRDReport,
        module='document_collection', type_='report')
    Pool.register(
        TRD,
        DocumentRecordInventory,
        DocumentSelect,
        module='document_collection', type_='wizard')
