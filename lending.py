#This file is part of Tryton.  The COPYRIGHT file at the top level
#of this repository contains the full copyright notices and license terms.
from __future__ import with_statement
from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.pyson import Eval
from trytond.pool import Pool, PoolMeta

__all__ = ['DocumentLending', 'DocumentLendingDocument']
__metaclass__ = PoolMeta

STATES = {
    'readonly': (Eval('state') != 'draft'),
}


class DocumentLending(Workflow, ModelSQL, ModelView):
    "Document Lending"
    __name__ = 'document.lending'
    _rec_name = 'number'
    number = fields.Char('Number', readonly=True)
    employee = fields.Many2One('company.employee', 'Employee', select=True,
            required=True, states=STATES)
    delivery_date = fields.Date('Delivery Date', required=True, states=STATES)
    return_date = fields.Date('Return Date', select=True, readonly=True)
    documents = fields.Many2Many('document.lending-document.document',
            'lending', 'document', 'Documents Lendings',
            domain=['OR', [
                    ('lending', '=', Eval('active_id', -1)),
                    ], [
                    ('lending', '=', None),
            ],],
            states=STATES)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('lent', 'Lent'),
            ('returned', 'Returned'),
            ('canceled', 'Canceled'),
        ], 'State', readonly=True, select=True)
    comment = fields.Text('Comment', states=STATES)

    @classmethod
    def __setup__(cls):
        super(DocumentLending, cls).__setup__()
        cls._order.insert(0, ('delivery_date', 'DESC'))
        # cls._error_messages.update({
        #         'document_not_available': ('Document not available: %s'),
        #         'not_documents': ('There is not documents to lending!'),
        #         })
        cls._transitions |= set((
            ('draft', 'lent'),
            ('lent', 'returned'),
            ('draft', 'cancel'),
        ))
        cls._buttons.update({
            'cancel': {
                'invisible': Eval('state') != 'draft'
            },
            'draft': {
                'invisible': Eval('state') != 'lent',
            },
            'lend': {
                'invisible': Eval('state') != 'draft',
            },
            'returned': {
                'invisible': Eval('state') != 'lent',
            },
        })

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_delivery_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, lendings):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('canceled')
    def cancel(cls, lendings):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('lent')
    def lend(cls, lendings):
        Document = Pool().get('document.document')
        for lending in lendings:
            lending.set_number()
            if not lending.documents:
                cls.raise_user_error('not_documents',)
            for doc in lending.documents:
                if doc.lending:
                    cls.raise_user_error(
                        'document_not_available', (doc.code,)
                    )
                Document.write([doc], {'lending': lending.id})

    @classmethod
    @ModelView.button
    @Workflow.transition('returned')
    def returned(cls, lendings):
        pool = Pool()
        Date = pool.get('ir.date')
        Document = pool.get('document.document')
        for lending in lendings:
            for doc in lending.documents:
                Document.write([doc], {'lending': None})
            cls.write([lending], {'return_date': Date.today()})

    def set_number(self):
        if self.number:
            return
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Configuration = pool.get('document.configuration')
        sequence = Configuration(1)
        seq = sequence.document_lending_sequence.id
        self.write([self], {'number': Sequence.get_id(seq)})


class DocumentLendingDocument(ModelSQL):
    "Document Lending Document"
    __name__ = 'document.lending-document.document'
    _table = 'document_lending_document_document'
    lending = fields.Many2One('document.lending', 'Lending',
           ondelete='CASCADE', select=True, required=True)
    document = fields.Many2One('document.document', 'Document',
             select=True, required=True)
