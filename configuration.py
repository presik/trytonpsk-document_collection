# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta

__all__ = ['Configuration']


class Configuration(metaclass=PoolMeta):
    __name__ = 'document.configuration'
    document_lending_sequence = fields.Many2One('ir.sequence',
        'Document Lending Sequence',
        domain=[
            ('code', '=', 'document.document')
        ])

    @classmethod
    def __setup__(cls):
        super(Configuration, cls).__setup__()
