# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import os
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval
from trytond.pool import Pool

__all__ = ['CategoryDocument']

STATES = {
    'invisible': Eval('kind') == 'base',
}


def fmt_pad(val, pad):
    tmp = '{0:0%dd}' % pad
    return tmp.format(val)


class CategoryDocument(ModelSQL, ModelView):
    "Category Document"
    __name__ = "document.category"
    name = fields.Char('Name', required=True)
    parent = fields.Many2One('document.category', 'Parent', select=True,
            states=STATES)
    childs = fields.One2Many('document.category', 'parent',
            string='Children')
    code = fields.Char('Code', select=True, states={
        'required': Eval('kind') != 'base',
    })
    responsible = fields.Many2One('company.employee', 'Responsible',
            states={
                'required': Eval('kind') == 'section',
                'invisible': ~Eval('kind').in_(['section', 'subsection']),
            })
    retention_time = fields.Integer('Retention Time AG (Years)', select=True,
            states={
                'invisible': ~Eval('kind').in_(['series', 'subseries']),
            }, help="In years")
    retention_time_central = fields.Integer('Retention Time AC (Years)', select=True,
            states={
                'invisible': ~Eval('kind').in_(['series', 'subseries']),
            }, help="In years")
    kind = fields.Selection([
            ('base', 'Base'),
            ('section', 'Section'),
            ('subsection', 'Subsection'),
            ('series', 'Series'),
            ('subseries', 'Subseries'),
        ], 'Kind', required=True)
    next_number = fields.Integer('Next Number', select=True,
            states={
                'required': Eval('kind') == 'series',
                'invisible': Eval('kind') != 'series',
            })
    padding = fields.Integer('Padding', select=True,
        states={
            'required': Eval('kind') == 'base',
            'invisible': Eval('kind') != 'base',
        })
    path = fields.Function(fields.Char('Path'), 'get_path')

    @classmethod
    def __setup__(cls):
        super(CategoryDocument, cls).__setup__()
        cls._order.insert(0, ('code', 'ASC'))
        # cls._error_messages.update({
        #     'missing_padding': 'Missing to define padding on base!',
        #     'directory_no_empty': 'The directory is not empty!',
        #     'missing_company_config': 'Missing company configuration!',
        #     'not_possible_remove_dir': 'It is not possible remove directory: %s',
        #     'not_possible_rename_dir': 'It is not possible rename directory!',
        #     'not_possible_create_dir': 'It is not possible create directory!',
        # })

    @staticmethod
    def default_next_number():
        return 1

    # @classmethod
    # def validate(cls, categories):
    #     super(CategoryDocument, cls).validate(categories)

    def get_rec_name(self, name):
        if self.parent:
            return self.parent.get_rec_name(name) + '/' + self.name
        else:
            return self.name

    def get_next_number(self):
        padding = self.get_base_padding()
        res = fmt_pad(self.next_number, padding)
        self.write([self], {
            'next_number': (self.next_number + 1),
        })
        return res

    def get_path(self, name=None):
        path_base_company = CategoryDocument.get_path_base_company()
        if path_base_company:
            return os.path.join(path_base_company, self.rec_name)

    def get_base_padding(self):
        padding = 0
        parent = self.parent
        if parent and parent.kind:
            while parent.kind != 'base':
                parent = parent.parent
        if not hasattr(parent, 'padding') or not parent.padding:
            return padding
        return parent.padding

    @classmethod
    def search_rec_name(cls, name, clause):
        if isinstance(clause[2], basestring):
            values = clause[2].split('/')
            values.reverse()
            domain = []
            field = 'name'
            for name in values:
                domain.append((field, clause[1], name.strip()))
                field = 'parent.' + field
        else:
            domain = [('name',) + tuple(clause[1:])]
        ids = [w.id for w in cls.search(domain, order=[])]
        return [('parent', 'child_of', ids)]

    @classmethod
    def get_path_base_company(cls):

        Configuration = Pool().get('document.configuration')
        config = Configuration.get_configuration()
        if config.path_home is None:
            cls.raise_user_error('missing_path_dms')
        if not config.company_name_path:
            cls.raise_user_error('missing_company_config')

        return os.path.join(config.path_home, config.company_name_path)

    @classmethod
    def create(cls, values):
        for value in values:
            cls._create_directory(value)
        record_ids = super(CategoryDocument, cls).create(values)
        return record_ids

    @classmethod
    def _create_directory(cls, val):
        if val['parent']:
            path_parent = cls(val['parent']).path
        else:
            path_parent = cls.get_path_base_company()
        path_dir = os.path.join(path_parent, val['name']).encode('utf-8')
        if not os.path.exists(path_dir):
            # try:
            if 1:
                os.mkdir(path_dir, 0755)
            else: #except:
                cls.raise_user_error('not_possible_create_dir')

    @classmethod
    def write(cls, records, values):
        for rec in records:
            if values.get('name') or values.get('parent'):
                cls._rename_directory(rec, values.get('name'), values.get('parent'))
        super(CategoryDocument, cls).write(records, values)

    @classmethod
    def _rename_directory(cls, rec, name, parent):
        if not parent:
            if rec.parent:
                parent = rec.parent.path
            else:
                parent = cls.get_path_base_company()
        else:
            parent = cls(parent).path

        if not name:
            name = rec.name
        new_path = os.path.join(parent, name).encode('utf-8')
        rec_path = rec.path.encode('utf-8')
        if os.path.exists(rec_path):
            try:
                os.rename(rec_path, new_path)
            except:
                cls.raise_user_error('not_possible_rename_dir')

    @classmethod
    def delete(cls, records):
        for record in records:
            cls._remove_directory(record)
        super(CategoryDocument, cls).delete(records)

    @classmethod
    def _remove_directory(cls, rec):
        if os.path.exists(rec.path):
            try:
                os.rmdir(rec.path)
            except Exception as e:
                if e.errno == 39:
                    cls.raise_user_error('directory_no_empty')
                else:
                    cls.raise_user_error('not_possible_remove_dir', e)
