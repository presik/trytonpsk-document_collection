# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields

__all__ = ['DocumentLocation']


class DocumentLocation(ModelSQL, ModelView):
    "Document Location"
    __name__ = "document.location"
    name = fields.Char('Name', required=True)
    parent = fields.Many2One('document.location', 'Parent', select=True)
    childs = fields.One2Many('document.location', 'parent', string='Children')

    @classmethod
    def __setup__(cls):
        super(DocumentLocation, cls).__setup__()
        cls._order.insert(0, ('name', 'ASC'))

    @classmethod
    def validate(cls, locations):
        super(DocumentLocation, cls).validate(locations)

    def get_rec_name(self, name):
        if self.parent:
            return self.parent.get_rec_name(name) + ' / ' + self.name
        else:
            return self.name
